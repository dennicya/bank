package com.bank.account.mapper.core;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bank.account.domain.core.AccountWithdrawal;
import com.bank.account.domain.core.AccountWithdrawalExample;

public interface AccountWithdrawalMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	long countByExample(AccountWithdrawalExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int deleteByExample(AccountWithdrawalExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int deleteByPrimaryKey(Long id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int insert(AccountWithdrawal record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int insertSelective(AccountWithdrawal record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	List<AccountWithdrawal> selectByExample(AccountWithdrawalExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	AccountWithdrawal selectByPrimaryKey(Long id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByExampleSelective(@Param("record") AccountWithdrawal record, @Param("example") AccountWithdrawalExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByExample(@Param("record") AccountWithdrawal record, @Param("example") AccountWithdrawalExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByPrimaryKeySelective(AccountWithdrawal record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_withdrawals
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByPrimaryKey(AccountWithdrawal record);
}