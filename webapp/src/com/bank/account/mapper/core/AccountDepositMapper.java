package com.bank.account.mapper.core;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bank.account.domain.core.AccountDeposit;
import com.bank.account.domain.core.AccountDepositExample;

public interface AccountDepositMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	long countByExample(AccountDepositExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int deleteByExample(AccountDepositExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int deleteByPrimaryKey(Long id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int insert(AccountDeposit record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int insertSelective(AccountDeposit record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	List<AccountDeposit> selectByExample(AccountDepositExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	AccountDeposit selectByPrimaryKey(Long id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByExampleSelective(@Param("record") AccountDeposit record, @Param("example") AccountDepositExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByExample(@Param("record") AccountDeposit record, @Param("example") AccountDepositExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByPrimaryKeySelective(AccountDeposit record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.account_deposits
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	int updateByPrimaryKey(AccountDeposit record);
}