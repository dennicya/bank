package com.bank.account.services.impl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.account.domain.core.Account;
import com.bank.account.domain.core.AccountDeposit;
import com.bank.account.domain.core.AccountDepositExample;
import com.bank.account.domain.core.AccountExample;
import com.bank.account.domain.core.AccountWithdrawal;
import com.bank.account.domain.core.AccountWithdrawalExample;
import com.bank.account.mapper.core.AccountDepositMapper;
import com.bank.account.mapper.core.AccountMapper;
import com.bank.account.mapper.core.AccountWithdrawalMapper;
import com.bank.account.services.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountMapper accountMapper;

	@Autowired
	private AccountDepositMapper accountDepositMapper;

	@Autowired
	private AccountWithdrawalMapper accountWithdrawalMapper;

	@Override
	public BigDecimal accountBalance(String account) {

		if (account != null) {

			AccountExample example = new AccountExample();
			example.createCriteria().andAccountNumberEqualTo(account);

			List<Account> accounts = accountMapper.selectByExample(example);

			if (accounts.size() > 0) {

				return accounts.get(0).getAvailableBalance();
			}

		}

		return null;
	}

	@Override
	public Map<String, Object> deposit(Map<String, Object> inputs) {

		if (inputs != null) {

			String accountNumber = inputs.get("account") == null ? null : inputs.get("account").toString();
			BigDecimal amount = inputs.get("amount") == null ? null : new BigDecimal(inputs.get("amount").toString());

			Map<String, Object> data = new HashMap<String, Object>();

			if (accountNumber != null && amount != null) {

				Account account = fetchAccount(accountNumber);

				if (account != null) {

					if (amount.compareTo(account.getMaxTransDeposit()) > 0) {
						data.put("success", false);
						data.put("message", "The transaction amount is greater than the allowed transaction limit");

						return data;
					}

					AccountDepositExample example = new AccountDepositExample();
					example.createCriteria().andCreatedAtBetween(dayStart(), dayEnd());

					List<AccountDeposit> deposits = accountDepositMapper.selectByExample(example);

					if (deposits != null) {
						if (deposits.size() >= account.getMaxDailyDepositFreq()) {
							data.put("success", false);
							data.put("message", "You have exceeded the maximum allowed transaction limit of " + account.getMaxDailyDepositFreq());

							return data;
						} else {

							BigDecimal totalDeposits = amount;

							for (AccountDeposit accountDeposit : deposits) {
								totalDeposits.add(accountDeposit.getDepositAmount());
							}

							if (totalDeposits.compareTo(account.getMaxDailyDeposit()) > 0) {

								data.put("success", false);
								data.put("message", "You have exceeded the maximum allowed daily deposit amount of " + account.getMaxDailyDeposit());

								return data;
							}
						}
					}

					AccountDeposit accountDeposit = new AccountDeposit();

					accountDeposit.setAccountId(account.getId());
					accountDeposit.setInitialBalance(account.getAvailableBalance());
					accountDeposit.setDepositAmount(amount);
					accountDeposit.setDeleted(false);
					accountDeposit.setCreatedAt(new Date().getTime());
					accountDeposit.setUpdatedAt(new Date().getTime());

					accountDepositMapper.insertSelective(accountDeposit);

					BigDecimal balance = account.getAvailableBalance().add(amount);
					account.setAvailableBalance(balance);

					accountMapper.updateByPrimaryKeySelective(account);

					data.put("success", true);
					data.put("message", "Deposit successful");
					data.put("balance", account.getAvailableBalance());

					return data;
				} else {
					data.put("success", false);
					data.put("message", "The Account does not exist");

					return data;
				}
			}
		}
		return null;

	}

	@Override
	public Map<String, Object> withdraw(Map<String, Object> inputs) {

		if (inputs != null) {

			String accountNumber = inputs.get("account") == null ? null : inputs.get("account").toString();
			BigDecimal amount = inputs.get("amount") == null ? null : new BigDecimal(inputs.get("amount").toString());

			Map<String, Object> data = new HashMap<String, Object>();

			if (accountNumber != null && amount != null) {

				Account account = fetchAccount(accountNumber);

				if (account != null) {

					if (amount.compareTo(account.getMaxTransWithdraw()) > 0) {
						data.put("success", false);
						data.put("message", "The transaction amount is greater than the allowed transaction limit");

						return data;
					}

					if (account != null) {

						AccountWithdrawalExample example = new AccountWithdrawalExample();
						example.createCriteria().andCreatedAtBetween(dayStart(), dayEnd());

						List<AccountWithdrawal> withdrawals = accountWithdrawalMapper.selectByExample(example);

						if (withdrawals != null) {

							if (withdrawals.size() >= account.getMaxDailyWithdrawFreq()) {
								data.put("success", false);
								data.put("message", "You have exceeded the maximum allowed transaction limit of " + account.getMaxDailyWithdrawFreq());

								return data;
							} else {

								BigDecimal totalWithdrawal = amount;

								for (AccountWithdrawal accountWithdrawal : withdrawals) {
									totalWithdrawal.add(accountWithdrawal.getWithdrawalAmount());
								}

								if (totalWithdrawal.compareTo(account.getMaxDailyWithdraw()) > 0) {

									data.put("success", false);
									data.put("message", "You have exceeded the maximum allowed daily withdrawal amount of " + account.getMaxDailyWithdraw());

									return data;
								}
							}
						}

						if (account.getAvailableBalance().compareTo(new BigDecimal(0)) > 0 && account.getAvailableBalance().compareTo(amount) >= 0) {

							AccountWithdrawal accountWithdrawal = new AccountWithdrawal();

							accountWithdrawal.setAccountId(account.getId());
							accountWithdrawal.setInitialBalance(account.getAvailableBalance());
							accountWithdrawal.setWithdrawalAmount(amount);
							accountWithdrawal.setDeleted(false);
							accountWithdrawal.setCreatedAt(new Date().getTime());
							accountWithdrawal.setUpdatedAt(new Date().getTime());

							accountWithdrawalMapper.insertSelective(accountWithdrawal);

							BigDecimal balance = account.getAvailableBalance().subtract(amount);
							account.setAvailableBalance(balance);

							accountMapper.updateByPrimaryKeySelective(account);

							data.put("success", true);
							data.put("message", "Withdrawal successful");
							data.put("balance", account.getAvailableBalance());

							return data;

						} else {

							data.put("success", true);
							data.put("message", "You do not have sufficient balance to complete this transaction");
							data.put("balance", account.getAvailableBalance());

							return data;
						}
					}
				} else {
					data.put("success", false);
					data.put("message", "The Account does not exist");

					return data;
				}
			}
		}
		return null;
	}

	private Account fetchAccount(String account) {

		if (account != null) {

			AccountExample example = new AccountExample();
			example.createCriteria().andAccountNumberEqualTo(account);

			List<Account> accounts = accountMapper.selectByExample(example);

			if (accounts.size() > 0) {

				return accounts.get(0);
			}

		}

		return null;
	}

	private long dayStart() {

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.MILLISECOND, 00);

		return calendar.getTimeInMillis();

	}

	private long dayEnd() {

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.MILLISECOND, 59);

		return calendar.getTimeInMillis();

	}

}
