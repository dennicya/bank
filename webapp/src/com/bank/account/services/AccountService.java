package com.bank.account.services;

import java.math.BigDecimal;
import java.util.Map;

public interface AccountService {

	BigDecimal accountBalance(String account);

	Map<String, Object> deposit(Map<String, Object> inputs);

	Map<String, Object> withdraw(Map<String, Object> inputs);

}
