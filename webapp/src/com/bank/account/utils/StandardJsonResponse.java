package com.bank.account.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class StandardJsonResponse {

	private Boolean success = false;

	private String message = "";

	private String title = "";

	private Map<String, String> messages = new HashMap<String, String>();

	private Map<String, String> errors = new HashMap<String, String>();

	private Map<String, Object> data = new HashMap<String, Object>();

	private Map<String, Object> records = new HashMap<String, Object>();

	private Object record = null;

	private String targetUrl = "";

	public void addMessage(String field, String message) {
		this.messages.put(field, message);
	}

	public void addError(String field, String message) {
		this.errors.put(field, message);
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Map<String, String> getMessages() {
		return messages;
	}

	public void setMessages(Map<String, String> messages) {
		this.messages = messages;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	public void setErrors(ResourceBundle resourceBundle, Map<String, String> errors) {
		Map<String, String> errs = new HashMap<String, String>();
		for (String key : errors.keySet()) {
			errs.put(key, resourceBundle.getString(errors.get(key)));
		}
		this.errors = errs;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String getTargetUrl() {
		return targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public Map<String, Object> getRecords() {
		return records;
	}

	public void setRecords(Map<String, Object> records) {
		this.records = records;
	}

	public Object getRecord() {
		return record;
	}

	public void setRecord(Object record) {
		this.record = record;
	}

}