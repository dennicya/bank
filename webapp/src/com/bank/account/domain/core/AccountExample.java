package com.bank.account.domain.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public AccountExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Long value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Long value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Long value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Long value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Long value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Long value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Long> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Long> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Long value1, Long value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Long value1, Long value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("name is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("name is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("name =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("name <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("name >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("name >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("name <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("name <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("name like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("name not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("name in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("name not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("name between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("name not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andAccountNumberIsNull() {
			addCriterion("account_number is null");
			return (Criteria) this;
		}

		public Criteria andAccountNumberIsNotNull() {
			addCriterion("account_number is not null");
			return (Criteria) this;
		}

		public Criteria andAccountNumberEqualTo(String value) {
			addCriterion("account_number =", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberNotEqualTo(String value) {
			addCriterion("account_number <>", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberGreaterThan(String value) {
			addCriterion("account_number >", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberGreaterThanOrEqualTo(String value) {
			addCriterion("account_number >=", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberLessThan(String value) {
			addCriterion("account_number <", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberLessThanOrEqualTo(String value) {
			addCriterion("account_number <=", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberLike(String value) {
			addCriterion("account_number like", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberNotLike(String value) {
			addCriterion("account_number not like", value, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberIn(List<String> values) {
			addCriterion("account_number in", values, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberNotIn(List<String> values) {
			addCriterion("account_number not in", values, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberBetween(String value1, String value2) {
			addCriterion("account_number between", value1, value2, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andAccountNumberNotBetween(String value1, String value2) {
			addCriterion("account_number not between", value1, value2, "accountNumber");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositIsNull() {
			addCriterion("max_daily_deposit is null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositIsNotNull() {
			addCriterion("max_daily_deposit is not null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositEqualTo(BigDecimal value) {
			addCriterion("max_daily_deposit =", value, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositNotEqualTo(BigDecimal value) {
			addCriterion("max_daily_deposit <>", value, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositGreaterThan(BigDecimal value) {
			addCriterion("max_daily_deposit >", value, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("max_daily_deposit >=", value, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositLessThan(BigDecimal value) {
			addCriterion("max_daily_deposit <", value, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositLessThanOrEqualTo(BigDecimal value) {
			addCriterion("max_daily_deposit <=", value, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositIn(List<BigDecimal> values) {
			addCriterion("max_daily_deposit in", values, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositNotIn(List<BigDecimal> values) {
			addCriterion("max_daily_deposit not in", values, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_daily_deposit between", value1, value2, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_daily_deposit not between", value1, value2, "maxDailyDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositIsNull() {
			addCriterion("max_trans_deposit is null");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositIsNotNull() {
			addCriterion("max_trans_deposit is not null");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositEqualTo(BigDecimal value) {
			addCriterion("max_trans_deposit =", value, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositNotEqualTo(BigDecimal value) {
			addCriterion("max_trans_deposit <>", value, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositGreaterThan(BigDecimal value) {
			addCriterion("max_trans_deposit >", value, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("max_trans_deposit >=", value, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositLessThan(BigDecimal value) {
			addCriterion("max_trans_deposit <", value, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositLessThanOrEqualTo(BigDecimal value) {
			addCriterion("max_trans_deposit <=", value, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositIn(List<BigDecimal> values) {
			addCriterion("max_trans_deposit in", values, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositNotIn(List<BigDecimal> values) {
			addCriterion("max_trans_deposit not in", values, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_trans_deposit between", value1, value2, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxTransDepositNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_trans_deposit not between", value1, value2, "maxTransDeposit");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqIsNull() {
			addCriterion("max_daily_deposit_freq is null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqIsNotNull() {
			addCriterion("max_daily_deposit_freq is not null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqEqualTo(Integer value) {
			addCriterion("max_daily_deposit_freq =", value, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqNotEqualTo(Integer value) {
			addCriterion("max_daily_deposit_freq <>", value, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqGreaterThan(Integer value) {
			addCriterion("max_daily_deposit_freq >", value, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqGreaterThanOrEqualTo(Integer value) {
			addCriterion("max_daily_deposit_freq >=", value, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqLessThan(Integer value) {
			addCriterion("max_daily_deposit_freq <", value, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqLessThanOrEqualTo(Integer value) {
			addCriterion("max_daily_deposit_freq <=", value, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqIn(List<Integer> values) {
			addCriterion("max_daily_deposit_freq in", values, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqNotIn(List<Integer> values) {
			addCriterion("max_daily_deposit_freq not in", values, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqBetween(Integer value1, Integer value2) {
			addCriterion("max_daily_deposit_freq between", value1, value2, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyDepositFreqNotBetween(Integer value1, Integer value2) {
			addCriterion("max_daily_deposit_freq not between", value1, value2, "maxDailyDepositFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawIsNull() {
			addCriterion("max_daily_withdraw is null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawIsNotNull() {
			addCriterion("max_daily_withdraw is not null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawEqualTo(BigDecimal value) {
			addCriterion("max_daily_withdraw =", value, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawNotEqualTo(BigDecimal value) {
			addCriterion("max_daily_withdraw <>", value, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawGreaterThan(BigDecimal value) {
			addCriterion("max_daily_withdraw >", value, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("max_daily_withdraw >=", value, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawLessThan(BigDecimal value) {
			addCriterion("max_daily_withdraw <", value, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawLessThanOrEqualTo(BigDecimal value) {
			addCriterion("max_daily_withdraw <=", value, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawIn(List<BigDecimal> values) {
			addCriterion("max_daily_withdraw in", values, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawNotIn(List<BigDecimal> values) {
			addCriterion("max_daily_withdraw not in", values, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_daily_withdraw between", value1, value2, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_daily_withdraw not between", value1, value2, "maxDailyWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawIsNull() {
			addCriterion("max_trans_withdraw is null");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawIsNotNull() {
			addCriterion("max_trans_withdraw is not null");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawEqualTo(BigDecimal value) {
			addCriterion("max_trans_withdraw =", value, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawNotEqualTo(BigDecimal value) {
			addCriterion("max_trans_withdraw <>", value, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawGreaterThan(BigDecimal value) {
			addCriterion("max_trans_withdraw >", value, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("max_trans_withdraw >=", value, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawLessThan(BigDecimal value) {
			addCriterion("max_trans_withdraw <", value, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawLessThanOrEqualTo(BigDecimal value) {
			addCriterion("max_trans_withdraw <=", value, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawIn(List<BigDecimal> values) {
			addCriterion("max_trans_withdraw in", values, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawNotIn(List<BigDecimal> values) {
			addCriterion("max_trans_withdraw not in", values, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_trans_withdraw between", value1, value2, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxTransWithdrawNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("max_trans_withdraw not between", value1, value2, "maxTransWithdraw");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqIsNull() {
			addCriterion("max_daily_withdraw_freq is null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqIsNotNull() {
			addCriterion("max_daily_withdraw_freq is not null");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqEqualTo(Integer value) {
			addCriterion("max_daily_withdraw_freq =", value, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqNotEqualTo(Integer value) {
			addCriterion("max_daily_withdraw_freq <>", value, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqGreaterThan(Integer value) {
			addCriterion("max_daily_withdraw_freq >", value, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqGreaterThanOrEqualTo(Integer value) {
			addCriterion("max_daily_withdraw_freq >=", value, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqLessThan(Integer value) {
			addCriterion("max_daily_withdraw_freq <", value, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqLessThanOrEqualTo(Integer value) {
			addCriterion("max_daily_withdraw_freq <=", value, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqIn(List<Integer> values) {
			addCriterion("max_daily_withdraw_freq in", values, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqNotIn(List<Integer> values) {
			addCriterion("max_daily_withdraw_freq not in", values, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqBetween(Integer value1, Integer value2) {
			addCriterion("max_daily_withdraw_freq between", value1, value2, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andMaxDailyWithdrawFreqNotBetween(Integer value1, Integer value2) {
			addCriterion("max_daily_withdraw_freq not between", value1, value2, "maxDailyWithdrawFreq");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceIsNull() {
			addCriterion("available_balance is null");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceIsNotNull() {
			addCriterion("available_balance is not null");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceEqualTo(BigDecimal value) {
			addCriterion("available_balance =", value, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceNotEqualTo(BigDecimal value) {
			addCriterion("available_balance <>", value, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceGreaterThan(BigDecimal value) {
			addCriterion("available_balance >", value, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("available_balance >=", value, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceLessThan(BigDecimal value) {
			addCriterion("available_balance <", value, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("available_balance <=", value, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceIn(List<BigDecimal> values) {
			addCriterion("available_balance in", values, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceNotIn(List<BigDecimal> values) {
			addCriterion("available_balance not in", values, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("available_balance between", value1, value2, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andAvailableBalanceNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("available_balance not between", value1, value2, "availableBalance");
			return (Criteria) this;
		}

		public Criteria andDeletedIsNull() {
			addCriterion("deleted is null");
			return (Criteria) this;
		}

		public Criteria andDeletedIsNotNull() {
			addCriterion("deleted is not null");
			return (Criteria) this;
		}

		public Criteria andDeletedEqualTo(Boolean value) {
			addCriterion("deleted =", value, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedNotEqualTo(Boolean value) {
			addCriterion("deleted <>", value, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedGreaterThan(Boolean value) {
			addCriterion("deleted >", value, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedGreaterThanOrEqualTo(Boolean value) {
			addCriterion("deleted >=", value, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedLessThan(Boolean value) {
			addCriterion("deleted <", value, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedLessThanOrEqualTo(Boolean value) {
			addCriterion("deleted <=", value, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedIn(List<Boolean> values) {
			addCriterion("deleted in", values, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedNotIn(List<Boolean> values) {
			addCriterion("deleted not in", values, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedBetween(Boolean value1, Boolean value2) {
			addCriterion("deleted between", value1, value2, "deleted");
			return (Criteria) this;
		}

		public Criteria andDeletedNotBetween(Boolean value1, Boolean value2) {
			addCriterion("deleted not between", value1, value2, "deleted");
			return (Criteria) this;
		}

		public Criteria andCreatedAtIsNull() {
			addCriterion("created_at is null");
			return (Criteria) this;
		}

		public Criteria andCreatedAtIsNotNull() {
			addCriterion("created_at is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedAtEqualTo(Long value) {
			addCriterion("created_at =", value, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtNotEqualTo(Long value) {
			addCriterion("created_at <>", value, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtGreaterThan(Long value) {
			addCriterion("created_at >", value, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtGreaterThanOrEqualTo(Long value) {
			addCriterion("created_at >=", value, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtLessThan(Long value) {
			addCriterion("created_at <", value, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtLessThanOrEqualTo(Long value) {
			addCriterion("created_at <=", value, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtIn(List<Long> values) {
			addCriterion("created_at in", values, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtNotIn(List<Long> values) {
			addCriterion("created_at not in", values, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtBetween(Long value1, Long value2) {
			addCriterion("created_at between", value1, value2, "createdAt");
			return (Criteria) this;
		}

		public Criteria andCreatedAtNotBetween(Long value1, Long value2) {
			addCriterion("created_at not between", value1, value2, "createdAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtIsNull() {
			addCriterion("updated_at is null");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtIsNotNull() {
			addCriterion("updated_at is not null");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtEqualTo(Long value) {
			addCriterion("updated_at =", value, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtNotEqualTo(Long value) {
			addCriterion("updated_at <>", value, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtGreaterThan(Long value) {
			addCriterion("updated_at >", value, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtGreaterThanOrEqualTo(Long value) {
			addCriterion("updated_at >=", value, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtLessThan(Long value) {
			addCriterion("updated_at <", value, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtLessThanOrEqualTo(Long value) {
			addCriterion("updated_at <=", value, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtIn(List<Long> values) {
			addCriterion("updated_at in", values, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtNotIn(List<Long> values) {
			addCriterion("updated_at not in", values, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtBetween(Long value1, Long value2) {
			addCriterion("updated_at between", value1, value2, "updatedAt");
			return (Criteria) this;
		}

		public Criteria andUpdatedAtNotBetween(Long value1, Long value2) {
			addCriterion("updated_at not between", value1, value2, "updatedAt");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table core.accounts
	 * @mbg.generated  Sun Sep 18 11:06:56 EAT 2016
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table core.accounts
     *
     * @mbg.generated do_not_delete_during_merge Fri Sep 16 14:02:42 EAT 2016
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}