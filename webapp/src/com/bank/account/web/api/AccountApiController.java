package com.bank.account.web.api;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bank.account.services.AccountService;
import com.bank.account.utils.StandardJsonResponse;
import com.bank.account.web.BaseController;

@Controller
@RequestMapping("/api/account")
public class AccountApiController extends BaseController {

	@Autowired
	private AccountService accountService;

	@RequestMapping(value = "/balance.do", method = RequestMethod.POST)
	public @ResponseBody StandardJsonResponse checkBalance(@RequestBody Map<String, Object> inputs) {

		StandardJsonResponse responseObject = new StandardJsonResponse();
		try {
			if (inputs != null) {

				Map<String, Object> resp = new HashMap<String, Object>();

				String account = inputs.get("account") == null ? null : inputs.get("account").toString();

				if (account != null) {

					BigDecimal balance = accountService.accountBalance(account);

					if (balance != null) {
						responseObject.setSuccess(true);
						resp.put("balance", balance);
					} else {
						responseObject.setSuccess(false);
						responseObject.setMessage("The account does not exist");
					}

				} else {

					responseObject.setSuccess(false);
					responseObject.setMessage("An Account Number is required to query the balance");
				}

				responseObject.setData(resp);

			}
		} catch (Exception e) {
			responseObject.setSuccess(false);
			responseObject.setMessage(e.getMessage());
		}
		return responseObject;

	}

	@RequestMapping(value = "/deposit.do", method = RequestMethod.POST)
	public @ResponseBody StandardJsonResponse deposit(@RequestBody Map<String, Object> inputs, HttpServletRequest request) {

		StandardJsonResponse responseObject = new StandardJsonResponse();
		try {
			if (inputs != null) {

				Map<String, Object> resp = accountService.deposit(inputs);

				responseObject.setSuccess((Boolean) resp.get("success"));

				responseObject.setData(resp);

			}
		} catch (Exception e) {
			responseObject.setSuccess(false);
			responseObject.setMessage(e.getMessage());
		}
		return responseObject;

	}

	@RequestMapping(value = "/withdraw.do", method = RequestMethod.POST)
	public @ResponseBody StandardJsonResponse withdraw(@RequestBody Map<String, Object> inputs) {

		StandardJsonResponse responseObject = new StandardJsonResponse();
		try {
			if (inputs != null) {

				Map<String, Object> resp = accountService.withdraw(inputs);

				responseObject.setSuccess((Boolean) resp.get("success"));

				responseObject.setData(resp);

			}
		} catch (Exception e) {
			responseObject.setSuccess(false);
			responseObject.setMessage(e.getMessage());
		}
		return responseObject;
	}

}
