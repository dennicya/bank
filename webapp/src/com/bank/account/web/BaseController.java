package com.bank.account.web;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.bank.account.utils.StandardJsonResponse;


public class BaseController extends MultiActionController {

	protected final Log logger = LogFactory.getLog(getClass());
	
	/**
	 * Handles an operation that has resulted in an Exception.
	 * 
	 * @param response
	 *            Map holding the message
	 * @param e
	 * @return
	 */
	protected ModelAndView marshalResponse(StandardJsonResponse responseObject, Exception e) {
		logError(e.getMessage(), e);

		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put("success", responseObject.getSuccess());

		if (!responseObject.getMessage().equalsIgnoreCase("")) {
			responseMap.put("message", responseObject.getMessage());
		}

		if (!responseObject.getTitle().equalsIgnoreCase("")) {
			responseMap.put("title", responseObject.getTitle());
		}

		if (responseObject.getMessages().size() > 0) {
			responseMap.put("messages", responseObject.getMessages());
		}

		if (responseObject.getErrors().size() > 0) {
			responseMap.put("errors", responseObject.getErrors());
		}

		if (responseObject.getData().size() > 0) {
			responseMap.put("data", responseObject.getData());
		}

		if (responseObject.getRecord() != null) {
			responseMap.put("record", responseObject.getRecord());
		}

		return new ModelAndView("jsonView", responseMap);
	}

	/**
	 * Writes out exception stack trace to log file.
	 * 
	 * @param e
	 *            Exception
	 */
	protected void logError(String message, Exception e) {
		logger.error("Exception occurred: " + message, e);
	}

}
