package com.bank.account.web.main;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bank.account.web.BaseController;

@Controller
@RequestMapping("/")
public class IndexController extends BaseController {

	@RequestMapping("")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {

		try {

			Map<String, Object> model = new HashMap<String, Object>();

			model.put("title", "Bank Account App");

			return new ModelAndView("index", model);

		} catch (Exception e) {
			logError(e.getMessage(), e);
		}

		return null;
	}

}
