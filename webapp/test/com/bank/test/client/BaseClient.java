package com.bank.test.client;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.bank.account.utils.StandardJsonResponse;
import com.google.gson.Gson;

public class BaseClient {

	protected static String host = "localhost:8081";

	protected static StandardJsonResponse postRequest(String endpoint, Object obj) throws Exception {

		final HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		final RequestBuilder requestBuilder = RequestBuilder.post();
		final RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();

		Gson gson = new Gson();

		URI uri = new URIBuilder().setScheme("http").setHost(host).setPath(endpoint).build();
		requestBuilder.setUri(uri);

		HttpClientContext httpClientContext = HttpClientContext.create();

		if (obj != null) {

			String jsonBody = gson.toJson(obj);

			HttpEntity entity = new StringEntity(jsonBody, ContentType.APPLICATION_JSON.withCharset(Charset.forName("utf-8")));
			requestBuilder.setEntity(entity);
		}

		ResponseHandler<StandardJsonResponse> responseHandler = new ResponseHandler<StandardJsonResponse>() {

			@Override
			public StandardJsonResponse handleResponse(final HttpResponse response) throws IOException {

				StatusLine statusLine = response.getStatusLine();
				HttpEntity entity = response.getEntity();

				if (statusLine.getStatusCode() >= 300) {
					throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
				}
				if (entity == null) {
					throw new ClientProtocolException("Response contains no content");
				}

				ContentType contentType = ContentType.getOrDefault(entity);
				Charset charset = contentType.getCharset();
				Reader reader = new InputStreamReader(entity.getContent(), charset);
				return gson.fromJson(reader, StandardJsonResponse.class);
			}
		};

		RequestConfig requestConfig = requestConfigBuilder.build();
		requestBuilder.setConfig(requestConfig);

		HttpClient httpClient = httpClientBuilder.build();
		HttpUriRequest request = requestBuilder.build();

		StandardJsonResponse jsonResponse = httpClient.execute(request, responseHandler, httpClientContext);

		return jsonResponse;
	}

}
