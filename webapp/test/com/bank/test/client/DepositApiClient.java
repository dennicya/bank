package com.bank.test.client;

import java.util.HashMap;
import java.util.Map;

import com.bank.account.utils.StandardJsonResponse;
import com.google.gson.Gson;

public class DepositApiClient extends BaseClient {

	public static void main(String args[]) throws Exception {

		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("account", "A1012345");
		obj.put("amount", 20000.00);

		StandardJsonResponse balance = postRequest("/api/account/withdraw.do", obj);

		Gson gson = new Gson();

		System.out.println(gson.toJson(balance));

	}

}
