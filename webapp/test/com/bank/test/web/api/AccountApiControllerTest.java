package com.bank.test.web.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.bank.test.web.BaseControllerTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:WebContent/WEB-INF/config/spring/applicationContext-test.xml")
@WebAppConfiguration
public class AccountApiControllerTest extends BaseControllerTest {

	@Test
	public void testAccountBalance() throws Exception {

		MockHttpServletRequestBuilder balance = post("/api/account/balance.do").contentType(MediaType.APPLICATION_JSON).content("{'account':'12345'}");

		this.mockMvc.perform(balance).andDo(MockMvcResultHandlers.print()).andExpect(status().isOk());

	}

	@Test
	public void testAccountDeposit() throws Exception {

		MockHttpServletRequestBuilder deposit = post("/api/account/deposit.do").contentType(MediaType.APPLICATION_JSON)
				.content("{'account':'12345', 'amount':35000.00}");

		this.mockMvc.perform(deposit).andExpect(status().isOk());

	}

	@Test
	public void testAccountWithdrawal() throws Exception {

		MockHttpServletRequestBuilder createAccount = post("/api/account/withdraw.do").contentType(MediaType.APPLICATION_JSON)
				.content("{'account':'12345', 'amount':5000.00}");

		this.mockMvc.perform(createAccount).andExpect(status().isOk());

	}

}
