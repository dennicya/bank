<?xml version="1.0" encoding="UTF-8"?>

<project name="Bank" basedir="." default="build">
	<description>
        Ant build file for Bank
    </description>

	<!-- Set up the 'env' prefix for environment variables -->
	<property environment="env" />

	<property name="dist" value='dev' />
	<property file="filters/${dist}-filters.properties" />

	<property name="name" value="ROOT" />
	<property name="base.dir" value="." />
	<property name="java.src.dir" value="src" />
	<property name="java.test.dir" value="test" />
	<property name="webapp.base" value="WebContent" />
	<property name="war.file" value="${name}.war" />
	<property name="build.dir" value="build" />
	<property name="lib.dir" value="libs" />
	<property name="build.coverage.dir" value="${reports.dir}/coverage" />
	<property name="build.classes.dir" value="${build.dir}/classes" />
	<property name="config.dir" value="${webapp.base}/WEB-INF/config" />
	<property name="web.lib.dir" value="${webapp.base}/WEB-INF/lib" />
	<property name="maven.repo" value="../../maven_3rd_party_repo" />
	<property name="reports.dir" value="build/reports" />
	<property name="build.coverage.dir" value="${reports.dir}/coverage" />
	<property name="build.classes.dir" value="${build.dir}/classes" />

	<!-- Class path -->
	<path id="project.classpath">
		<pathelement location="${build.classes.dir}" />
		<fileset dir="${web.lib.dir}">
			<include name="**/*.jar" />
		</fileset>
		<fileset dir="${lib.dir}">
			<include name="*.jar" />
		</fileset>
		<fileset dir="${appserver.lib}">
			<include name="catalina-ant.jar" />
			<include name="tomcat-coyote.jar" />
			<include name="tomcat-util.jar" />
			<include name="servlet-api.jar" />
		</fileset>
	</path>

	<!--  Set the filter files to be used  -->

	<target name="set-filters" description="Initialize filters for main environment">
		<property name="filter.file" value="filters/${dist}-filters.properties" />
	</target>

	<target name="set-filters-test" description="Initialize filters for main environment">
		<property name="dist" value='test' />
		<property name="filter.file" value="filters/test-filters.properties" />
	</target>
	<!-- Set some output paths -->

	<target name="filter" description="Filter the config files">
		<echo message="Using filter file : ${filter.file}" />
		<filter filtersfile="${filter.file}" />
		<copy todir="${build.dir}/config" filtering="true">
			<fileset dir="config" />
		</copy>

		<copy todir="${config.dir}">
			<fileset dir="${build.dir}/config">
				<exclude name="**/web.xml" />
				<exclude name="**/views.xml" />
				<exclude name="**/log4j.properties" />
				<exclude name="**/generatorConfig.xml" />
			</fileset>
		</copy>

		<copy todir="${webapp.base}/WEB-INF">
			<fileset dir="${build.dir}/config">
				<include name="**/web.xml" />
				<include name="**/views.xml" />
			</fileset>
		</copy>

		<copy todir="${base.dir}">
			<fileset dir="${build.dir}/config">
				<include name="**/generatorConfig.xml" />
			</fileset>
		</copy>

	</target>

	<!-- Load project dependencies via Maven -->
	<target name="download-libs" xmlns:artifact="antlib:org.apache.maven.artifact.ant">
		<echo message="Loading JAR dependencies via Maven" />
		<artifact:localRepository id="maven.repo" path="${maven.repo}" />
		<artifact:pom id="maven.pom.file" file="pom.xml" />
		<artifact:dependencies pathId="compile.classpath" filesetId="mvn.jar.fileset">
			<pom refid="maven.pom.file" />
			<localRepository refid="maven.repo" />
		</artifact:dependencies>
		<copy todir="${lib.dir}/mvn" flatten="true">
			<fileset refid="mvn.jar.fileset" />
		</copy>
	</target>

	<!-- Set the main targets based on intended environment -->
	<target name="build" description="Compile and build application" depends="set-filters, filter, compile-java-src" />

	<!-- Compile Java source Files -->

	<target name="compile-java-src" description="Compile all java classes">
		<mkdir dir="${build.classes.dir}" />
		<javac srcdir="${java.src.dir}" includes="**" encoding="utf-8" destdir="${build.classes.dir}" nowarn="true" debug="true" debuglevel="lines,vars,source" includeAntRuntime="false">
			<classpath refid="project.classpath" />
		</javac>
		<copy todir="${build.classes.dir}">
			<fileset dir="${build.dir}/config">
				<include name="**/log4j.properties" />
				<include name="**/*.xml" />
			</fileset>
		</copy>
		<copy todir="${build.classes.dir}">
			<fileset dir="${java.src.dir}">
				<include name="**/*.xml" />
			</fileset>
		</copy>
	</target>

	<target name="clean" depends="clean-tests" description="Clean the project of compiled files">
		<delete>
			<fileset dir="${webapp.base}/WEB-INF" includes="**/views.xml" />
			<fileset dir="${webapp.base}/META-INF" includes="**/context.xml" />
			<fileset dir="${java.src.dir}" includes="**/log4j.properties" />
			<fileset dir="${webapp.base}/WEB-INF" includes="**/web.xml" />
			<fileset dir="${lib.dir}/mvn" includes="*.jar" excludes="README" />
		</delete>
		<delete dir="build" />
		<delete dir="${lib.shared.dir}" />
		<delete dir="${webapp.base}/WEB-INF/config" />
		<delete dir="${webapp.base}/classes" />
		<delete dir="${webapp.base}/WEB-INF/views/templates" />
		<delete dir="${webapp.base}/build" />
	</target>

	<target name="war" description="Create WAR file for deployment to production">
		<war destfile="build/${war.file}" webxml="${webapp.base}/WEB-INF/web.xml" basedir="${webapp.base}" excludes="build/**, apps/**, ext/**, packages/**, .*/**, *.*">
			<classes dir="${build.classes.dir}">
				<exclude name="**/test/**" />
			</classes>
		</war>
	</target>

	<target name="deploy" depends="deploy-classes" description="Deploy application" />

	<target name="deploy-classes" depends="build" description="Deploy application classes">
		<copy todir="${deploy.path}/${name}" preservelastmodified="true">
			<fileset dir="${webapp.base}">
				<include name="**/*.*" />
			</fileset>
		</copy>
		<copy todir="${deploy.path}/${name}/WEB-INF/classes" preservelastmodified="true">
			<fileset dir="${build.dir}/classes">
				<include name="**/*.*" />
				<exclude name="**/test/**" />
			</fileset>
		</copy>
	</target>

	<!-- BEGIN TESTS -->

	<target name="test" description="Compile and build application" depends="set-filters-test, filter, compile-java-tests-src" />

	<target name="clean-tests" description="Cleans the previously compiled test classses">
		<delete failonerror="false" includeemptydirs="true">
			<fileset dir="${build.dir}/config" includes="**/*.*" />
			<fileset dir="${webapp.base}/WEB-INF/config" includes="**/*.*" />
			<fileset dir="test" includes="**/*.class" />
			<fileset dir="${reports.dir}" includes="**/*.*" />
		</delete>
	</target>

	<target name="compile-java-tests-src" depends="compile-java-src" description="Compiles test code">
		<javac srcdir="${java.test.dir}" includes="**" encoding="utf-8" destdir="${build.classes.dir}" nowarn="true" debug="true" debuglevel="lines,vars,source" includeAntRuntime="false">
			<classpath refid="project.classpath" />
		</javac>
	</target>

	<!-- END TESTS -->

	<!-- ============================================================== -->
	<!-- Tomcat tasks -->
	<!-- ============================================================== -->

	<path id="catalina-ant-classpath">
		<fileset dir="${appserver.lib}">
			<include name="*.jar" />
		</fileset>
		<fileset dir="${appserver.home}/bin">
			<include name="tomcat-juli.jar" />
		</fileset>
	</path>

	<taskdef name="reload" classname="org.apache.catalina.ant.ReloadTask">
		<classpath refid="catalina-ant-classpath" />
	</taskdef>

	<taskdef name="start" classname="org.apache.catalina.ant.StartTask">
		<classpath refid="catalina-ant-classpath" />
	</taskdef>
	<taskdef name="stop" classname="org.apache.catalina.ant.StopTask">
		<classpath refid="catalina-ant-classpath" />
	</taskdef>

	<taskdef name="undeploy" classname="org.apache.catalina.ant.UndeployTask">
		<classpath refid="catalina-ant-classpath" />
	</taskdef>

	<taskdef name="list" classname="org.apache.catalina.ant.ListTask">
		<classpath refid="catalina-ant-classpath" />
	</taskdef>

	<target name="reload" description="Reload application in Tomcat">
		<reload url="${tomcat.manager.url}" username="${tomcat.manager.username}" password="${tomcat.manager.password}" path="/" />
	</target>

	<target name="start" description="Start Tomcat application">
		<start url="${tomcat.manager.url}" username="${tomcat.manager.username}" password="${tomcat.manager.password}" path="/" />
	</target>

	<target name="stop" description="Stop Tomcat application">
		<stop url="${tomcat.manager.url}" username="${tomcat.manager.username}" password="${tomcat.manager.password}" path="/" />
	</target>

	<target name="undeploy" description="Remove web application">
		<undeploy url="${tomcat.manager.url}" username="${tomcat.manager.username}" password="${tomcat.manager.password}" path="/" />
	</target>

	<target name="list" description="List Tomcat applications">
		<list url="${tomcat.manager.url}" username="${tomcat.manager.username}" password="${tomcat.manager.password}" />
	</target>


	<!-- End Tomcat tasks -->

</project>
