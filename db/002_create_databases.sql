
CREATE DATABASE bank_development
  WITH OWNER = "bank-dbadmin"
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;
	   
CREATE DATABASE bank_test
  WITH OWNER = "bank-dbadmin"
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;