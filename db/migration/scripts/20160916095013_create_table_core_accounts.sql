--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create_table_core_accounts
-- Migration SQL that makes the change goes here.

CREATE TABLE core.accounts
(
  id bigserial NOT NULL PRIMARY KEY,
  name character varying(64) NOT NULL,
  account_number character varying(64) NOT NULL,
  max_daily_deposit numeric(12,2) NOT NULL DEFAULT 0.0,
  max_trans_deposit numeric(12,2) NOT NULL DEFAULT 0.0,
  max_daily_deposit_freq int NOT NULL DEFAULT 0,
  max_daily_withdraw numeric(12,2) NOT NULL DEFAULT 0.0,
  max_trans_withdraw numeric(12,2) NOT NULL DEFAULT 0.0,
  max_daily_withdraw_freq int NOT NULL DEFAULT 0,
  available_balance numeric(12,2) NOT NULL DEFAULT 0.0,
  deleted boolean NOT NULL DEFAULT false,
  created_at bigint NOT NULL DEFAULT 0,
  updated_at bigint NOT NULL DEFAULT 0
)
WITH (
  OIDS=FALSE
);

-- //@UNDO
-- SQL to undo the change goes here.

DROP TABLE core.accounts;
